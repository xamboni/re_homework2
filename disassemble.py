
# Homework 2
# Reverse Engineering and Vulnerability Analysis
# Alexander Morgan

# x86 Disassembler Program
# Supported Mnemonics:
#  add, and, call, cmp, dec, idiv, imul, inc, jmp,
#  jz/jnz, lea, mov, movsd, mul, neg, nop, not, or,
#  pop, push, repne cmpsd, *retf*, retn, sal, sar, sbb
#  shr, test, xor

import sys

# Opens the file in same dir "instructions.bin" -- Deprecated

input_file = "examples.bin"

registers = { 0: "eax",
              1: "ecx",
              2: "edx",
              3: "ebx",
              4: "esp",
              5: "ebp",
              6: "esi",
              7: "edi" }

# global labels list. Add in if you can
labels = []

# Debug Prints
printModRM = False
printImm32 = False
printImm16 = False
printDecodeRegister = False

def createWord(instructions, index):
        word = [instructions[index], 
                 instructions[index + 1]] 
        return word

def createDword(instructions, index):
        dword = [instructions[index], 
                 instructions[index + 1], 
                 instructions[index + 2], 
                 instructions[index + 3]]
        return dword    

def createImm16(word):
    imm16 = 0
    imm16 |= word[0]
    imm16 |= (word[1] << 8)

    if printImm16:
        print "Immediate 16: " + str(hex(imm16))
    return imm16

def createImm32(dword):
    imm32 = 0
    imm32 |= dword[0]
    imm32 |= (dword[1] << 8)
    imm32 |= (dword[2] << 16)
    imm32 |= (dword[3] << 24)

    if printImm32:
        print "Immediate 32: " + str(hex(imm32))
    return imm32

def decodeRegister(code):
    if printDecodeRegister:
        print registers[code]
    return registers[code]

def createSib(byte):
    pass

def createOperands(mode, instructions, index, increment, regBits, rmBits):
    if mode == 0:
        # [E?X]
        rmOperand = "dword ptr [" + registers[rmBits] + "]" 
        regOperand = registers[regBits]

    elif mode == 1:
        # [E?X + disp8]
        disp8 = instructions[index + increment]

        if disp8 > 127:
            disp8 = (256-disp8) * (-1)
        disp8 = str(hex(disp8))
        rmOperand = "dword ptr [" + registers[rmBits] + " + " + disp8 + "]"
        increment += 1
        regOperand = registers[regBits]

    elif mode == 2:
        # [E?X + disp32]
        dword = createDword(instructions, index + increment)
        disp32 = createImm32(dword)

        if disp32 > 2147483647:
            disp32 = (4294967296 - disp32) * (-1)
        disp32 = str(hex(disp32))
        rmOperand = "[" + registers[rmBits] + " + " + disp32 + "]"
        increment += 4
        regOperand = registers[regBits]

    elif mode == 3:
        # Straight Register
        rmOperand = registers[rmBits]
        regOperand = registers[regBits]

    return regOperand, rmOperand, increment

# NOTE: Need to monitor for SIB byte and increment appropriately
def createModRM(byte):
    lowBits = byte & 0x07
    medBits = (byte & 0x38) >> 3
    mode = byte >> 6
    modRM = (mode, medBits, lowBits)

    if printModRM:
        print "Mod R/M: " + str(modRM)
    return modRM 

def disassemble(instructions, index):
    line_number = "0x" + hex(index)[2:].zfill(8)
    op = ""
    opcode = instructions[index]
    increment = 1

    # ADD r/m32, r32
    if (opcode == 0x01 or opcode == 0x21 or opcode == 0x39 
        or opcode == 0x89 or opcode == 0x09 or opcode == 0x19
        or opcode == 0x85 or opcode == 0x31):

        if opcode == 0x01:
            op += "add "
        elif opcode == 0x21:
            op += "and "
        elif opcode == 0x39:
            op += "cmp "
        elif opcode == 0x89:
            op += "mov "
        elif opcode == 0x09:
            op += "or "
        elif opcode == 0x19:
            op += "sbb "
        elif opcode == 0x85:
            op += "test "
        elif opcode == 0x31:
            op += "xor "
        else:
            print "BUG in 0x01, 0x21, 0x39, 0x89."
        mode, regBits, rmBits = createModRM(instructions[index + increment])
        increment += 1

        reg32, rm32, increment = createOperands(mode, instructions,
                                                index, increment, 
                                                regBits, rmBits)

        # Put it together
        op += (rm32 + ", ")
        op += (reg32)
 
    # ADD r32, r/m32
    elif (opcode == 0x03 or opcode == 0x23
             or opcode == 0x3B or opcode == 0x8B
             or opcode == 0x0B or opcode == 0x1B
             or opcode == 0x33):
        if opcode == 0x03:
            op += "add "
        elif opcode == 0x23:
            op += "and "
        elif opcode == 0x3B:
            op += "cmp "
        elif opcode == 0x8B:
            op += "mov "
        elif opcode == 0x0B:
            op += "or "
        elif opcode == 0x1B:
            op += "sbb "
        elif opcode == 0x33:
            op += "xor "
        else:
            print "BUG in 0x03, 0x23, 0x3B."
        mode, regBits, rmBits = createModRM(instructions[index + increment])
        increment += 1

        reg32, rm32, increment = createOperands(mode, instructions,
                                                index, increment, 
                                                regBits, rmBits)

        # Put it together
        op += (reg32 + ", ")
        op += (rm32)

    # ADD eax, imm32
    elif (opcode == 0x05 or opcode == 0x25 or opcode == 0x3D 
             or opcode == 0x0D or opcode == 0x1D or opcode == 0xA9
             or opcode == 0x35):
        if opcode == 0x05:
            op += "add eax, "
        elif opcode == 0x25:
            op += "and eax, "
        elif opcode == 0x3D:
            op += "cmp eax, "
        elif opcode == 0x0D:
            op += "or eax, "
        elif opcode == 0x0D:
            op += "sbb eax, "
        elif opcode == 0xA9:
            op += "test eax, "
        elif opcode == 0x35:
            op += "xor eax, "
        else:
            print "Bug: 0x05, 0x25, 0x3D."

        dword = createDword(instructions, index + increment)
        imm32 = str(hex(createImm32(dword)))
        op += imm32 + " "
        increment += 4

    # ADD r/m32, imm32
    # AND r/m32, imm32
    elif opcode == 0x81:
        # Must check ModRM to find out which instruction
        mode, regBits, rmBits = createModRM(instructions[index + increment])
        increment += 1

        if regBits == 0:
            op += "add "
        elif regBits == 1:
            op += "or "
        elif regBits == 3:
            op += "sbb " 
        elif regBits == 4:
            op += "and "
        elif regBits == 6:
            op += "xor "
        elif regBits == 7:
            op += "cmp "
        else:
            print "Bug in 0x81."

        reg32, rm32, increment = createOperands(mode, instructions,
                                                index, increment, 
                                                regBits, rmBits)

        op += rm32 + ", "

        dword = createDword(instructions, index + increment)
        imm32 = str(hex(createImm32(dword)))
        op += imm32 
        increment += 4

    # mov r32, imm32
    elif opcode >= 0xB8 and opcode <= 0xBF:
        op += "mov "
        op += registers[ (opcode - 0xB8) ] + " "

        dword = createDword(instructions, index + increment)
        imm32 = str(hex(createImm32(dword)))
        op += imm32 
        increment += 4

    # call rel32
    elif opcode == 0xE8:
        op += "call "

        dword = createDword(instructions, index + increment)
        imm32 = str(hex(createImm32(dword)))
        increment += 4

        offset = ((index + increment) + int(imm32, 16)) & 0xFFFFFFFF
        label = "offset_" + str( hex(offset)[2:] ) + "h"
        labels.append(label)
        op += label

    # jmp rel32
    elif opcode == 0xE9:
        op += "jmp "

        dword = createDword(instructions, index + increment)
        imm32 = str(hex(createImm32(dword)))
        increment += 4

        offset = ((index + increment) + int(imm32, 16)) & 0xFFFFFFFF
        label = "offset_" + str( hex(offset)[2:] ) + "h"
        labels.append(label)
        op += label

    # call r/m32
    # jmp r/m32
    elif opcode == 0xFF:
        mode, regBits, rmBits = createModRM(instructions[index + increment])
        increment += 1

        if regBits == 2:
            op += "call "
        elif regBits == 4:
            op += "jmp "
        elif regBits == 1:
            op += "dec "
        elif regBits == 0:
            op += "inc "
        elif regBits == 6:
            op += "push "
        else:
            print "Bug in 0xFF."

        reg32, rm32, increment = createOperands(mode, instructions,
                                                index, increment, 
                                                regBits, rmBits)

        # Put it together
        op += rm32 

    # pop r/m32
    elif opcode == 0x8F:
        mode, regBits, rmBits = createModRM(instructions[index + increment])
        increment += 1

        if regBits == 0:
            op += "pop "
        else:
            print "Bug in 0x8F."

        reg32, rm32, increment = createOperands(mode, instructions,
                                                index, increment, 
                                                regBits, rmBits)

        # Put it together
        op += rm32 

    # Special Pop
    elif opcode == 0x1F or opcode == 0x07 or opcode == 0x17:
        if opcode == 0x1F:
            op += "pop ds"
        elif opcode == 0x07:
            op += "pop es"
        elif opcode == 0x17:
            op += "pop ss"
        else:
            print "BUG in 0x1F, 0x07, 0x17."

    # push imm32
    elif opcode == 0x68:
        op += "push "        
        dword = createDword(instructions, index + increment)
        imm32 = str(hex(createImm32(dword)))
        op += imm32 

    # Special Push
    elif opcode == 0x0E or opcode == 0x16 or opcode == 0x1E:
        if opcode == 0x0E:
            op += "push cs"
        elif opcode == 0x16:
            op += "push ss"
        elif opcode == 0x1E:
            op += "push es"
        else:
            print "BUG in 0x0E, 0x16, 0x1E."

    # dec + reg
    elif opcode >= 0x48 and opcode <= 0x4F:
        op += "dec "
        op += registers[opcode - 0x48]

    # inc + reg
    elif opcode >= 0x40 and opcode <= 0x47:
        op += "inc "
        op += registers[opcode - 0x40]

    # pop + reg
    elif opcode >= 0x58 and opcode <= 0x5F:
        op += "pop "
        op += registers[opcode - 0x58]

    # push + reg
    elif opcode >= 0x50 and opcode <= 0x57:
        op += "push "
        op += registers[opcode - 0x50]

    # idiv and imul
    elif opcode == 0xF7:
        mode, regBits, rmBits = createModRM(instructions[index + increment])
        increment += 1

        if regBits == 0x07:
            op += "idiv "
        elif regBits == 0x05:
            op += "imul "
        elif regBits == 0x04:
            op += "mul "
        elif regBits == 0x03:
            op += "neg "
        elif regBits == 0x02:
            op += "not "
        elif regBits == 0x00:
            op += "test "
        else:
            print "Bug in 0xF7."
    
        reg32, rm32, increment = createOperands(mode, instructions,
                                                index, increment, 
                                                regBits, rmBits)

        op += rm32 

    # imul
    elif opcode == 0x69:
        op += "imul "
        mode, regBits, rmBits = createModRM(instructions[index + increment])
        increment += 1

        reg32, rm32, increment = createOperands(mode, instructions,
                                                index, increment, 
                                                regBits, rmBits)

        op += (reg32 + ", ")
        op += (rm32 + " , ")

        dword = createDword(instructions, index + increment)
        imm32 = str(hex(createImm32(dword)))
        op += imm32 
        increment += 4

    # nop
    elif opcode == 0x90:
        op += "nop "

    # lea
    elif opcode == 0x8D:
        op += "lea "
        mode, regBits, rmBits = createModRM(instructions[index + increment])
        increment += 1

        reg32, rm32, increment = createOperands(mode, instructions,
                                                index, increment, 
                                                regBits, rmBits)

        # Put it together
        op += (reg32 + ", ")
        op += (rm32)

    # movsd, repne cmpsd
    elif opcode == 0xF2:
        opcode2 = instructions[index + increment]
        increment += 1

        if opcode2 == 0x0F:
            op += "movsd "
            opcode3 = instructions[index + increment]
            # Increment for opcode3
            increment += 1
            mode, regBits, rmBits = createModRM(instructions[index + increment])
            increment += 1
    
            reg32, rm32, increment = createOperands(mode, instructions,
                                                    index, increment, 
                                                    regBits, rmBits)

            if opcode3 == 0x10:
                # Put it together
                op += (reg32 + ", ")
                op += (rm32)

            elif opcode3 == 0x11:
                # Put it together
                op += (rm32 + ", ")
                op += (reg32)

        elif opcode2 == 0xA7:
            op += "repne cmps "
            increment += 1
        else:
            print "BUG in 0xF2."

    # retf and retf imm16
    elif opcode == 0xCB or opcode == 0xCA:
        if opcode == 0xCB:
            op += "retf"
        elif opcode == 0xCA:
            op += "retf"
            word = createWord(instructions, index + increment)
            imm16 = str(hex(createImm16(word)))
            op += imm16 
            increment += 2
        else:
            print "Bug in retf."

    # retn and retn imm16
    elif opcode == 0xC3 or opcode == 0xC2:
        if opcode == 0xC3:
            op += "ret"
        elif opcode == 0xC2:
            op += "ret "
            word = createWord(instructions, index + increment)
            imm16 = str(hex(createImm16(word)))
            op += imm16 
            increment += 2
        else:
            print "Bug in retn."

    # sal, sar, shr
    elif opcode == 0xD1 or opcode == 0xD3:
        mode, regBits, rmBits = createModRM(instructions[index + increment])
        increment += 1

        if regBits == 0x04:
            op += "sal "
        elif regBits == 0x07:
            op += "sar "
        elif regBits == 0x05:
            op += "shr "
        else:
            print "BUG: 0xD1, 0xD3."

        # Shift once or by CL
        if opcode == 0xD1:
            pass
        elif opcode == 0xD3:
            op += "cl"
        else:
            print "BUG: 0xD1, 0xD3."

    elif opcode == 0x75:
        op += "jnz "
        # add one byte of offset
        rel8 = hex(instructions[index + increment])
        increment += 1
        op += rel8

    elif opcode == 0x74:
        op += "jz "
        # add one byte of offset
        rel8 = hex(instructions[index + increment])
        increment += 1
        offset = ((index + increment) + int(rel8, 16)) & 0xFFFFFFFF
        label = "offset_" + str( hex(offset)[2:] ) + "h"
        labels.append(label)
        op += label

    # Two byte opcodes
    # 0x0F AF
    # 0x0F 85
    # 0x0F 84
    #0x0F 1F 
    #0x0F A1 
    #0x0F A9 
    #0x0F A0 
    #0x0F A8 
    elif opcode == 0x0F:
        opcode2 = instructions[index + increment]
        increment += 1

        noModRM = False

        if opcode2 == 0xAF:
            op += "imul "
        elif opcode2 == 0x85:
            op += "jnz "
            # add four bytes of offset
            dword = createDword(instructions, index + increment)
            imm32 = str(hex(createImm32(dword)))
            increment += 4

            offset = ((index + increment) + int(imm32, 16)) & 0xFFFFFFFF
            label = "offset_" + str( hex(offset)[2:] ) + "h"
            labels.append(label)
            op += label

            noModRM = True
        elif opcode2 == 0x84:
            op += "jz "
            # add four bytes of offset
            dword = createDword(instructions, index + increment)
            imm32 = str(hex(createImm32(dword)))
            increment += 4

            offset = ((index + increment) + int(imm32, 16)) & 0xFFFFFFFF
            label = "offset_" + str( hex(offset)[2:] ) + "h"
            labels.append(label)
            op += label

            noModRM = True
        elif opcode2 == 0x1F:
            op += "nop "
            noModRM = True
        elif opcode2 == 0xA1:
            op += "pop fs"
            noModRM = True
        elif opcode2 == 0xA9:
            op += "pop ds"
            noModRM = True
        elif opcode2 == 0xA0:
            op += "push fs"
            noModRM = True
        elif opcode2 == 0xA8:
            op += "push gs"
            noModRM = True
        else:
            print "BUG in 0x0F."

        if not noModRM:
            mode, regBits, rmBits = createModRM(instructions[index + increment])
            increment += 1
    
            reg32, rm32, increment = createOperands(mode, instructions,
                                                    index, increment, 
                                                    regBits, rmBits)
    
            # Put it together
            op += (reg32 + ", ")
            op += (rm32)

    # Invalid opcode
    else:
        op += "Invalid Opcode."

    totalOp = ""
    for i in range(0, increment):
        totalOp += str(hex(instructions[index + i])) + " "

    if op != "Invalid Opcode.":
        print line_number + " %-60s %s" % (str(totalOp), op)
        output_file.write(line_number + " %-60s %s" % (str(totalOp), op) + "\n")
    # Return the amount to increment the byte count
    return op, increment

if __name__ == "__main__":
    instructions = None
    # Open file for disassebly
    for arg in sys.argv:
        input_file = arg
    instFile = input_file
    with open(instFile, "rb") as inputFile:
        output_file = open("output.txt", "w")
        instructions = bytearray(inputFile.read())
        index = 0
        while index < len(instructions):
            op, increment = disassemble(instructions, index)
            index += increment

print labels
output_file.close()

with open("output_with_labels.txt", "w") as f:
    output_old = open("output.txt", "r")
    lines = output_old.readlines()
    for label in labels:
        labelStart = label.find('_') + 1
        labelEnd = label.find('h')
        labelGuts = label[labelStart:labelEnd]
        zpadLabel = labelGuts.zfill(8)
        # print zpadLabel

        for line in lines:
            if line[2:10] == zpadLabel:
                f.write(label + ":\n")
            f.write(line)
                

